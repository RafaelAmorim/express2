#!/bin/bash

docker stop express1_test

# Delete the old repo
rm -rf /home/ec2-user/express1_test

# any future command that fails will exit the script
set -e

# BE SURE TO UPDATE THE FOLLOWING LINE WITH THE URL FOR YOUR REPO
git clone https://gitlab.com/RafaelAmorim/express1.git

cd /home/ec2-user/express1_test

git checkout test

# run the node app in a container
deploy/express1_test.sh -d

#!/bin/bash
docker run --network isolated_nw --name express1_test --rm $1 \
        -v /home/ec2-user/express1_test:/mnt/stuff \
        -w /mnt/stuff node:alpine npm run firststart

1 - clone repo with ssh: git@gitlab.com:RafaelAmorim/express1.git

2- generate your pem key

3 - connect to the VM:
  chmod go-rwx ~/environment/YOUR_PRIVATE_KEY.pem # only need to do this once to fix permissions
  ssh -i ~/environment/YOUR_PRIVATE_KEY.pem ec2-user@app.494911.xyz

  Remember to change "YOUR_PRIVATE_KEY" with your pem key.

4 - Move inside the express1 folder: cd express1

5 - Start the app and copy nginx.conf files:
  deploy/express1.sh -d
  cp deploy/nginx.conf ~/etc/nginx/conf.d

6 - Start the app and copy nginx_test.conf files in the test environment:
  cd ../express1-test
  deploy/express1_test.sh -d
  cp deploy/nginx_test.conf ~/etc/nginx/conf.d

7 - Run nginx container:
  deploy/nginx.sh -d
